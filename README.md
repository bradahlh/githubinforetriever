# REST API to retrieve GitHub information about a certain project
API retrieves owner name, project name, name of top committer, number of commits top committer has made and all languages used in the project.

## Installation
To build:
```
go install bitbucket.org/bradahlh/githubinforetriever
```


## Instructions
1. Start server: `$GOPATH/bin/githubinforetriever`
2. In your web browser, enter the following basepath: http://localhost:8080/projectinfo/v1/ followed by an organization/username, and then a project name (for example http://localhost:8080/projectinfo/v1/kde/okular for KDE's Okular project)
3. The API should return JSON formatted information about the project in your web browser
