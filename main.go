package main

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	router.GET("/projectinfo/v1/:git/:owner/:project", gitHandler)
	http.ListenAndServe(":8080", router)
}