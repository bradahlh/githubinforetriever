package main

type Committer struct {
	Committer string   `json:"login"`
	Commits   int      `json:"contributions"`
}

type GitObject struct {
	Owner     string   `json:"owner"`
	Project   string   `json:"project"`
	Committer string   `json:"committer"`
	Commits   int      `json:"commits"`
	Languages []string `json:"language"`
}